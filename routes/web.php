<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

//Usuario
Route::resource('usuarios','App\Http\Controllers\UsuarioController');

//Año Lectivo
Route::resource('anio-lectivo','App\Http\Controllers\Ano_lectivoController');

//Horarios
Route::resource('horarios','App\Http\Controllers\HorarioController');

//Representantes
Route::resource('representantes','App\Http\Controllers\RepresentanteController');

//Distributivos
Route::resource('distributivos','App\Http\Controllers\DistributivoController');

//Empleados
Route::resource('empleados','App\Http\Controllers\EmpleadoController');

Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard.index');
})->name('dashboard');