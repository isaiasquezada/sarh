<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('ferep_codigo');
            $table->string('ferep_nombre', 80);
            $table->string('ferep_apelli', 80);
            $table->string('ferep_direcc', 80);
            $table->string('ferep_telefo', 45);
            $table->string('ferep_profes', 45);
            $table->string('ferep_observ', 80);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
