<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('feemp_codigo');
            $table->string('feemp_nombre', 80);
            $table->string('feemp_apelli', 80);
            $table->string('feemp_titulo', 60);
            $table->string('feemp_especi', 60);
            $table->string('feemp_funcio', 60);
            $table->string('feemp_cargo', 60);
            $table->string('feemp_imagen', 200)->default('default.png'); // campo para almacenar la ruta de la imagen
            $table->string('feemp_telefo', 15);
            $table->string('feemp_observ', 80);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
