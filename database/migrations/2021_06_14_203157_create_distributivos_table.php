<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributivos', function (Blueprint $table) {
            $table->increments('fedis_codigo');
            $table->string('fedis_nompla', 80);
            $table->string('fedis_telpla', 15);
            $table->string('fedis_anolec', 15);
            $table->string('fedis_lugar', 15);
            $table->string('fedis_especi', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributivos');
    }
}
