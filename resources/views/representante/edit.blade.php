@extends('dashboard.index')

@section('content_header')
    <h1>Editar Representante</h1>
@stop

@section('content')
<form action="/representantes/{{ $representante->ferep_codigo }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group"> <!-- Código -->
            <label for="ferep_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="ferep_codigo" name="ferep_codigo" value="{{ $representante->ferep_codigo }}" readonly>
    </div> 
    
    <div class="form-group"> <!-- Nombre -->
        <label for="ferep_nombre" class="control-label">Nombre</label>
        <input type="text" class="form-control" id="ferep_nombre" maxlength="80" name="ferep_nombre" value="{{ $representante->ferep_nombre }}" placeholder="Nombre del Representante">
    </div>

    <div class="form-group"> <!-- Apellido -->
        <label for="ferep_apelli" class="control-label">Apellido</label>
        <input type="text" class="form-control" id="ferep_apelli" maxlength="80" name="ferep_apelli" value="{{ $representante->ferep_apelli }}"placeholder="Apellido del Representante">
    </div>

    <div class="form-group"> <!-- Dirección -->
        <label for="ferep_direcc" class="control-label">Direcci&oacute;n</label>
        <input type="text" class="form-control" id="ferep_direcc" maxlength="80" name="ferep_direcc" value="{{ $representante->ferep_direcc }}" placeholder="Direcci&oacute;n">
    </div>

    <div class="form-group"> <!-- Teléfono -->
        <label for="ferep_telefo" class="control-label">Tel&eacute;fono</label>
        <input type="text" class="form-control" id="ferep_telefo" maxlength="15" name="ferep_telefo" value="{{ $representante->ferep_telefo }}" placeholder="Tel&eacute;fono">
    </div>

    <div class="form-group"> <!-- Profesión -->
        <label for="ferep_profes" class="control-label">Profesi&oacute;n</label>
        <input type="text" class="form-control" id="ferep_profes" maxlength="45" name="ferep_profes" value="{{ $representante->ferep_profes }}" placeholder="Profesi&oacute;n">
    </div>
                                                
    <div class="form-group"> <!-- Observaciones-->
            <label for="ferep_observ" class="control-label">Observaciones</label>
            <input type="textarea" class="form-control" id="ferep_observ" name="ferep_observ" value="{{ $representante->ferep_observ }}" placeholder="Observaciones">
    </div>

    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/representantes">Cancelar</a>
    </div>     
    
</form>       
@endsection