@extends('dashboard.index')

@section('content_header')
    <h1>Representantes</h1>
@stop

@section('content')
<a href="representantes/create" class="btn btn-primary">CREAR</a>
<table class="table table-dark table-striped mt-4">
    <thead>
        <tr>
            <th scope="col">C&oacute;digo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Direcci&oacute;n</th>
            <th scope="col">Tel&eacute;fono</th>
            <th scope="col">Profesi&oacute;n</th>
            <th scope="col">Observaci&oacute;n</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($representantes as $representante)
            <tr>
                <td>{{ $representante->ferep_codigo }}</td>
                <td>{{ $representante->ferep_nombre }}</td>
                <td>{{ $representante->ferep_apelli }}</td>
                <td>{{ $representante->ferep_direcc }}</td>
                <td>{{ $representante->ferep_telefo }}</td>
                <td>{{ $representante->ferep_profes }}</td>
                <td>{{ $representante->ferep_observ }}</td>
                <td><a href="/representantes/{{ $representante->ferep_codigo }}/edit" class="btn btn-info">Editar</a>&nbsp;|&nbsp;
                    <a href="#" 
                    data-id={{$representante->ferep_codigo}}
                    class="btn btn-danger delete" 
                    data-toggle="modal" 
                    data-target="#deleteModal">Eliminar</a>                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<!-- Ventana para confirmar la eliminación -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <h5 class="text-center">¿Est&aacute; seguro de eliminar este registro?</h5>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-danger">S&iacute;, Eliminar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Eliminación --> 
@endsection