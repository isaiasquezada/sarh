@extends('dashboard.index')

@section('content_header')
    <h1>Nuevo usuario</h1>
@stop

@section('content')
<form action="/usuarios" method="POST">
    @csrf
    <div class="form-group"> <!-- Código -->
            <label for="feusu_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="feusu_codigo" name="feusu_codigo" value="[ ? ]" readonly>
    </div>  

    <div class="form-group"> <!-- Identificacion -->
            <label for="feusu_identi" class="control-label">Identificaci&oacute;n</label>
            <input type="text" class="form-control" id="feusu_identi" name="feusu_identi" placeholder="Nombre de usuario">
    </div>                                  
                                                    
    <div class="form-group"> <!-- Password -->
            <label for="feusu_passwo" class="control-label">Contraseña</label>
            <input type="password" class="form-control" id="feusu_passwo" name="feusu_passwo" placeholder="Escriba su clave">
    </div>  

    <div class="form-group"> <!-- Observaciones-->
            <label for="feusu_observ" class="control-label">Observaciones</label>
            <input type="textarea" class="form-control" id="feusu_observ" name="feusu_observ" placeholder="Observaciones">
    </div>                                                                                                                     
    
    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/usuarios">Cancelar</a>
    </div>     
    
</form>       
@endsection