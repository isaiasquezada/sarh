@extends('dashboard.index')

@section('content_header')
    <h1>Empleados</h1>
@stop

@section('content')
<a href="empleados/create" class="btn btn-primary">CREAR</a>
<table class="table table-dark table-striped mt-4">
    <thead>
        <tr>
            <th scope="col">C&oacute;digo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">T&iacute;tulo</th>
            <th scope="col">Especializaci&oacute;n</th>
            <th scope="col">Funci&oacute;n</th>
            <th scope="col">Cargo</th>
            <th scope="col">Imagen</th>
            <th scope="col">Observaci&oacute;n</th>
            <th scope="col">Tel&eacute;fono</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($empleados as $empleado)
            <tr>
                <td>{{ $empleado->feemp_codigo }}</td>
                <td>{{ $empleado->feemp_nombre }}</td>
                <td>{{ $empleado->feemp_apelli }}</td>
                <td>{{ $empleado->feemp_titulo }}</td>
                <td>{{ $empleado->feemp_especi }}</td>
                <td>{{ $empleado->feemp_funcio }}</td>
                <td>{{ $empleado->feemp_cargo }}</td>
                <td>{{ $empleado->feemp_imagen }}</td>
                <td>{{ $empleado->feemp_observ }}</td>
                <td>{{ $empleado->feemp_telefo }}</td>              
                <td><a href="/empleados/{{ $empleado->feemp_codigo }}/edit" class="btn btn-info">Editar</a>&nbsp;|&nbsp;
                    <a href="#" 
                    data-id={{$empleado->feemp_codigo}}
                    class="btn btn-danger delete" 
                    data-toggle="modal" 
                    data-target="#deleteModal">Eliminar</a>                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<!-- Ventana para confirmar la eliminación -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Empleado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <h5 class="text-center">¿Est&aacute; seguro de eliminar este registro?</h5>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-danger">S&iacute;, Eliminar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Eliminación --> 
@endsection