@extends('dashboard.index')

@section('content_header')
    <h1>Nuevo Empleado</h1>
@stop

@section('content')
<form action="/empleados" method="POST">
    @csrf
    <div class="form-group"> <!-- Código -->
            <label for="feemp_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="feemp_codigo" name="feemp_codigo" value="[ ? ]" readonly>
    </div>  

    <div class="form-group"> <!-- Nombre -->
            <label for="feemp_nombre" class="control-label">Nombre</label>
            <input type="text" class="form-control" id="feemp_nombre" maxlength="80" name="feemp_nombre" placeholder="Nombre del empleado">
    </div>
    
    <div class="form-group"> <!-- Apellido -->
        <label for="feemp_apelli" class="control-label">Apellido</label>
        <input type="text" class="form-control" id="feemp_apelli" maxlength="80" name="feemp_apelli" placeholder="Apellido del empleado">
    </div>

    <div class="form-group"> <!-- Título -->
        <label for="feemp_titulo" class="control-label">T&iacute;tulo</label>
        <input type="text" class="form-control" id="feemp_titulo" maxlength="60" name="feemp_titulo" placeholder="T&iacute;itulo">
    </div>

    <div class="form-group"> <!-- Especialización -->
        <label for="feemp_especi" class="control-label">Especializaci&oacute;n</label>
        <input type="text" class="form-control" id="feemp_especi" maxlength="60" name="feemp_especi" placeholder="Especializaci&oacute;n">
    </div>

    <div class="form-group"> <!-- Función -->
        <label for="feemp_funcio" class="control-label">Funci&oacute;n</label>
        <input type="text" class="form-control" id="feemp_funcio" maxlength="60" name="feemp_funcio" placeholder="Funci&oacute;n">
    </div>

    <div class="form-group"> <!-- Cargo -->
        <label for="feemp_cargo" class="control-label">Cargo</label>
        <input type="text" class="form-control" id="feemp_cargo" maxlength="60" name="feemp_cargo" placeholder="Cargo">
    </div>

    <div class="form-group"> <!-- IMAGEN -->
        <label for="feemp_imagen" class="control-label">Imagen</label>
        <input type="text" class="form-control" id="feemp_imagen" maxlength="60" name="feemp_imagen" placeholder="Imagen">
    </div>

    <div class="form-group"> <!-- Teléfono -->
        <label for="feemp_telefo" class="control-label">Tel&eacute;fono</label>
        <input type="text" class="form-control" id="feemp_telefo" maxlength="15" name="feemp_telefo" placeholder="Tel&eacute;fono">
    </div>

    <div class="form-group"> <!-- Observaciones-->
            <label for="feemp_observ" class="control-label">Observaciones</label>
            <input type="textarea" class="form-control" id="feemp_observ" name="feemp_observ" placeholder="Observaciones">
    </div>                                                                                                                     
    
    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/empleados">Cancelar</a>
    </div>     
    
</form>       
@endsection