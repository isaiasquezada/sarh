@extends('dashboard.index')

@section('content_header')
    <h1>Nuevo Distributivo</h1>
@stop

@section('content')
<form action="/distributivos" method="POST">
    @csrf
    <div class="form-group"> <!-- Código -->
            <label for="fedis_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="fedis_codigo" name="fedis_codigo" value="[ ? ]" readonly>
    </div>  

    <div class="form-group"> <!-- Nombre de Plantel -->
            <label for="fedis_nompla" class="control-label">Nombre del Plantel</label>
            <input type="text" class="form-control" id="fedis_nompla" maxlength="80" name="fedis_nompla" placeholder="Nombre del Plantel">
    </div>
    
    <div class="form-group"> <!-- Teléfono -->
        <label for="fedis_telpla" class="control-label">Tel&eacute;fono</label>
        <input type="text" class="form-control" id="fedis_telpla" maxlength="15" name="fedis_telpla" placeholder="Tel&eacute;no">
    </div>
    
    <div class="form-group"> <!-- Año Lectivo -->
        <label for="fedis_" class="control-label">A&ntilde;o Lectivo</label>
        <input type="text" class="form-control" id="fedis_anolec" maxlength="15" name="fedis_anolec" placeholder="A&ntilde;o lectivo">
    </div>

    <div class="form-group"> <!-- Lugar -->
        <label for="fedis_" class="control-label">Lugar</label>
        <input type="text" class="form-control" id="fedis_lugar" maxlength="15" name="fedis_lugar" placeholder="Lugar">
    </div>

    <div class="form-group"> <!-- Especialización -->
        <label for="fedis_" class="control-label">Especializaci&oacute;n</label>
        <input type="text" class="form-control" id="fedis_especi" maxlength="15" name="fedis_especi" placeholder="Especializaci&oacute;n">
    </div>
    
    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/distributivos">Cancelar</a>
    </div>     
    
</form>       
@endsection