@extends('dashboard.index')

@section('content_header')
    <h1>Distributivos</h1>
@stop

@section('content')
<a href="distributivos/create" class="btn btn-primary">CREAR</a>
<table class="table table-dark table-striped mt-4">
    <thead>
        <tr>
            <th scope="col">C&oacute;digo</th>
            <th scope="col">Nombre del Plantel</th>
            <th scope="col">Tel&eacute;fono</th>
            <th scope="col">A&ntilde;o Lectivo</th>
            <th scope="col">Lugar</th>
            <th scope="col">Especializaci&oacute;n</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($distributivos as $distributivo)
            <tr>
                <td>{{ $distributivo->fedis_codigo }}</td>
                <td>{{ $distributivo->fedis_nompla }}</td>
                <td>{{ $distributivo->fedis_telpla }}</td>
                <td>{{ $distributivo->fedis_anolec }}</td>
                <td>{{ $distributivo->fedis_lugar }}</td>
                <td>{{ $distributivo->fedis_especi }}</td>
                <td><a href="/distributivos/{{ $distributivo->fedis_codigo }}/edit" class="btn btn-info">Editar</a>&nbsp;|&nbsp;
                    <a href="#" 
                    data-id={{$distributivo->fedis_codigo}}
                    class="btn btn-danger delete" 
                    data-toggle="modal" 
                    data-target="#deleteModal">Eliminar</a>                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<!-- Ventana para confirmar la eliminación -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <h5 class="text-center">¿Est&aacute; seguro de eliminar este registro?</h5>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-danger">S&iacute;, Eliminar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Eliminación --> 
@endsection