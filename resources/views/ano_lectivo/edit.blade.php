@extends('dashboard.index')

@section('content_header')
    <h1>Editar A&ntilde;o Lectivo</h1>
@stop

@section('content')
<form action="/anio-lectivo/{{ $anio_lectivo->feano_codigo }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group"> <!-- Código -->
            <label for="feano_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="feano_codigo" name="feano_codigo" value="{{ $anio_lectivo->feano_codigo }}" readonly>
    </div>  
    
    <div class="form-group"> <!-- Descripción -->
        <label for="feano_descri" class="control-label">A&ntilde;o</label>
        <input type="text" class="form-control" id="feano_descri" maxlength="15" name="feano_descri" required value="{{ $anio_lectivo->feano_descri }}">
    </div> 
                                                    
    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/anio-lectivo">Cancelar</a>
    </div>     
    
</form>       
@endsection