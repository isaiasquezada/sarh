@extends('dashboard.index')

@section('content_header')
    <h1>Nuevo Horario</h1>
@stop

@section('content')
<form action="/horarios" method="POST">
    @csrf
    <div class="form-group"> <!-- Código -->
            <label for="fehor_codigo" class="control-label">C&oacute;digo</label>
            <input type="text" class="form-control" id="fehor_codigo" name="fehor_codigo" value="[ ? ]" readonly>
    </div>  

    <div class="form-group"> <!-- Fecha -->
            <label for="fehor_fecha" class="control-label">Fecha</label>
            <input type="date" class="form-control" id="fehor_fecha" name="fehor_fecha" placeholder="Fecha">
    </div>                                  
                                                    
    <div class="form-group"> <!-- Observaciones-->
            <label for="fehor_observ" class="control-label">Observaciones</label>
            <input type="textarea" class="form-control" id="fehor_observ" name="fehor_observ" placeholder="Observaciones">
    </div>                                                                                                                     
    
    <div class="form-group"> <!-- Botón Guardar -->
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary">Limpiar</button>
            <a href="/horarios">Cancelar</a>
    </div>     
    
</form>       
@endsection