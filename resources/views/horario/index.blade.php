@extends('dashboard.index')

@section('content_header')
    <h1>Horarios</h1>
@stop

@section('content')
<a href="horarios/create" class="btn btn-primary">CREAR</a>
<table class="table table-dark table-striped mt-4">
    <thead>
        <tr>
            <th scope="col">C&oacute;digo</th>
            <th scope="col">Fecha</th>
            <th scope="col">Observaci&oacute;n</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($horarios as $horario)
            <tr>
                <td>{{ $horario->fehor_codigo }}</td>
                <td>{{ $horario->fehor_fecha }}</td>
                <td>{{ $horario->fehor_observ }}</td>
                <td><a href="/horarios/{{ $horario->fehor_codigo }}/edit" class="btn btn-info">Editar</a>&nbsp;|&nbsp;
                    <a href="#" 
                    data-id={{$horario->fehor_codigo}}
                    class="btn btn-danger delete" 
                    data-toggle="modal" 
                    data-target="#deleteModal">Eliminar</a>                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<!-- Ventana para confirmar la eliminación -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Horario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <h5 class="text-center">¿Est&aacute; seguro de eliminar este registro?</h5>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-danger">S&iacute;, Eliminar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Eliminación --> 
@endsection