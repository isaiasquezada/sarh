<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empleado;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleado::all();
        return view('empleado.index')->with('empleados',$empleados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empleado = new Empleado();
        $empleado->feemp_nombre = $request->get('feemp_nombre');
        $empleado->feemp_apelli = $request->get('feemp_apelli');
        $empleado->feemp_titulo = $request->get('feemp_titulo');
        $empleado->feemp_especi = $request->get('feemp_especi');
        $empleado->feemp_funcio = $request->get('feemp_funcio');
        $empleado->feemp_cargo = $request->get('feemp_cargo');
        $empleado->feemp_imagen = $request->get('feemp_imagen');
        $empleado->feemp_telefo = $request->get('feemp_telefo');
        $empleado->feemp_observ = $request->get('feemp_observ');

        $empleado->save();

        return redirect('/empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $feemp_codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($feemp_codigo)
    {
        $empleado = Empleado::find($feemp_codigo);
        return view('empleado.edit')->with('empleado', $empleado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empleado = Empleado::find($id);
        $empleado->feemp_nombre = $request->get('feemp_nombre');
        $empleado->feemp_apelli = $request->get('feemp_apelli');
        $empleado->feemp_titulo = $request->get('feemp_titulo');
        $empleado->feemp_especi = $request->get('feemp_especi');
        $empleado->feemp_funcio = $request->get('feemp_funcio');
        $empleado->feemp_cargo = $request->get('feemp_cargo');
        $empleado->feemp_imagen = $request->get('feemp_imagen');
        $empleado->feemp_telefo = $request->get('feemp_telefo');
        $empleado->feemp_observ = $request->get('feemp_observ');

        $empleado->save();

        return redirect('/empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleado::find($id);
        $empleado->delete();

        return redirect('/empleados');
    }
}
