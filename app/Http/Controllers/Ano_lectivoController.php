<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ano_lectivo;

class Ano_lectivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anio_lectivo = Ano_lectivo::all();
        return view('ano_lectivo.index')->with('anio_lectivo',$anio_lectivo);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ano_lectivo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anio_lectivo = new Ano_lectivo();
        $anio_lectivo->feano_descri = $request->get('feano_descri');

        $anio_lectivo->save();

        return redirect('/anio-lectivo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $feano_codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($feano_codigo)
    {
        $anio_lectivo = Ano_lectivo::find($feano_codigo);
        return view('ano_lectivo.edit')->with('anio_lectivo', $anio_lectivo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $anio_lectivo = Ano_Lectivo::find($id);
        $anio_lectivo->feano_descri = $request->get('feano_descri');

        $anio_lectivo->save();

        return redirect('/anio-lectivo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anio_lectivo = Ano_lectivo::find($id);
        $anio_lectivo->delete();

        return redirect('/anio-lectivo');
    }
}
