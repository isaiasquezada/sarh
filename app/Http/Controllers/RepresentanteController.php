<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Representante;

class RepresentanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $representantes = Representante::all();
        return view('representante.index')->with('representantes',$representantes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('representante.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $representante = new Representante();
        $representante->ferep_nombre = $request->get('ferep_nombre');
        $representante->ferep_apelli = $request->get('ferep_apelli');
        $representante->ferep_direcc = $request->get('ferep_direcc');
        $representante->ferep_telefo = $request->get('ferep_telefo');
        $representante->ferep_profes = $request->get('ferep_profes');
        $representante->ferep_observ = $request->get('ferep_observ');

        $representante->save();

        return redirect('/representantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $ferep_codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($ferep_codigo)
    {
        $representante = Representante::find($ferep_codigo);
        return view('representante.edit')->with('representante', $representante);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $representante = Representante::find($id);        
        $representante->ferep_nombre = $request->get('ferep_nombre');
        $representante->ferep_apelli = $request->get('ferep_apelli');
        $representante->ferep_direcc = $request->get('ferep_direcc');
        $representante->ferep_telefo = $request->get('ferep_telefo');
        $representante->ferep_profes = $request->get('ferep_profes');
        $representante->ferep_observ = $request->get('ferep_observ');

        $representante->save();

        return redirect('/representantes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $representante = Representante::find($id);
        $representante->delete();

        return redirect('/representantes');
    }
}
