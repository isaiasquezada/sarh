<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Distributivo;

class DistributivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distributivos = Distributivo::all();
        return view('distributivo.index')->with('distributivos',$distributivos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('distributivo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $distributivos = new Distributivo();
        $distributivos->fedis_nompla = $request->get('fedis_nompla');
        $distributivos->fedis_telpla = $request->get('fedis_telpla');
        $distributivos->fedis_anolec = $request->get('fedis_anolec');
        $distributivos->fedis_lugar = $request->get('fedis_lugar');
        $distributivos->fedis_especi = $request->get('fedis_especi');

        $distributivos->save();

        return redirect('/distributivos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $fedis_codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($fedis_codigo)
    {
        $distributivo = Distributivo::find($fedis_codigo);
        return view('distributivo.edit')->with('distributivo', $distributivo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $distributivo = Distributivo::find($id);
        $distributivo->fedis_nompla = $request->get('fedis_nompla');
        $distributivo->fedis_telpla = $request->get('fedis_telpla');
        $distributivo->fedis_anolec = $request->get('fedis_anolec');
        $distributivo->fedis_lugar = $request->get('fedis_lugar');
        $distributivo->fedis_especi = $request->get('fedis_especi');

        $distributivo->save();

        return redirect('/distributivos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distributivo = Distributivo::find($id);
        $distributivo->delete();

        return redirect('/distributivos');
    }
}
