<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        return view('usuario.index')->with('usuarios',$usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuarios = new Usuario();
        //$usuarios->feusu_codigo = $request->get('feusu_codigo');
        $usuarios->feusu_identi = $request->get('feusu_identi');
        $usuarios->feusu_passwo = $request->get('feusu_passwo');
        $usuarios->feusu_observ = $request->get('feusu_observ');

        $usuarios->save();

        return redirect('/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $feusu_codigo
     * @return \Illuminate\Http\Response
     */
    public function edit($feusu_codigo)
    {
        $usuario = Usuario::find($feusu_codigo);
        return view('usuario.edit')->with('usuario', $usuario);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);
        //$usuarios->feusu_codigo = $request->get('feusu_codigo');
        $usuario->feusu_identi = $request->get('feusu_identi');
        $usuario->feusu_passwo = $request->get('feusu_passwo');
        $usuario->feusu_observ = $request->get('feusu_observ');

        $usuario->save();

        return redirect('/usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();

        return redirect('/usuarios');
    }
}
