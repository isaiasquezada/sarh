<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ano_lectivo extends Model
{
    protected $table = 'ano_lectivo';
    protected $primaryKey = 'feano_codigo';

    use HasFactory;
}
